#!/bin/bash

# Verify we're running in bash:
test -n "$BASH" || { echo "Bash shell required to include bash-helpers." && exit 1 ; }

# Lists of various dependencies:
PXBH_UTILITIES_DEPENDENCIES=(
    tty
    tee
    dd
    tput
    shasum
    python
    git
)
PXBH_PYTHON_MODULES_DEPENDENCIES=(
    simplejson
    json
)

#==============================================================================
# Decorative functions
#==============================================================================

function echo_heading()
{
	STYLE_BEGIN=$(tty -s && tput bold && tput setaf 6) || STYLE_BEGIN="== "
	STYLE_NORMAL=$(tty -s && tput sgr0) || STYLE_NORMAL=" =="
	echo "${STYLE_BEGIN}$1${STYLE_NORMAL}"
}

function echo_bold()
{
	STYLE_BEGIN=$(tty -s && tput bold) || STYLE_BEGIN="*"
	STYLE_NORMAL=$(tty -s && tput sgr0) || STYLE_NORMAL="*"
	echo "${STYLE_BEGIN}$1${STYLE_NORMAL}"
}

function echo_error()
{
    STYLE_BEGIN=$(tty -s && tput bold && tput setaf 1) || STYLE_BEGIN=""
	STYLE_NORMAL=$(tty -s && tput sgr0) || STYLE_NORMAL=""
	echo "ERROR: ${STYLE_BEGIN}$1${STYLE_NORMAL}" >&2
}

#==============================================================================
# Logging functions
#==============================================================================

##
## Logs a message to a local file, or a remote logstash instance (JSON format).
##
function log()
{

    # If the log file is not defined, then print an error message:
    [[ -z "$LOG_FILE" ]] && echo_error "Log file not defined." && return

    local entry_date=$( date "+%Y-%m-%d %H:%M:%S" )
    local entry_message="$1"
    local entry_host=$( hostname )
    local entry_program=$( basename "$0" )
    local entry_pid="$$"
    local entry_body="[$entry_host] [$entry_program] [pid $entry_pid] [$entry_date]: $entry_message"

    echo "$entry_body" >>"$LOG_FILE"

    # Attempt to log to the remote logstash instance (json format):
    if [[ -n "$REMOTE_LOG_HOST" && -n "$REMOTE_LOG_PORT" ]]; then
        local json_entry="
        {
            \"message\": \"$entry_message\",
            \"program\": \"$entry_program\",
            \"pid\": \"$entry_pid\",
            \"logsource\": \"$entry_host\"
        }
        "
        echo "$json_entry" | nc -w 5 $REMOTE_LOG_HOST $REMOTE_LOG_PORT 1>/dev/null 2>/dev/null || echo_error "Remote logging failed."
    fi

}

##
## Redirects all further output from the given bash script to a local file.
## REMOVED DUE TO SYNTAX ERROR ISSUES CAUSED WHEN RUNNING FROM TASK SPOOLER (TS).
##
#function redirect_all_output_to_file()
#{
#    local _log_file="$1"
#    exec 1> >(tee -a "$_log_file")
#    exec 2>&1
#}

#==============================================================================
# Program flow functions
#==============================================================================

function ask_if_continue()
{
	echo -n "Continue? (y/n) [n]:"
	local _should_continue
	read -n 1 _should_continue
	echo
	if [[ $_should_continue != 'y' ]]; then
		echo "Script aborted by user."
		exit 1
	fi
}

function ask_affirmative_question()
{
    local _answer
    echo -n "$1 (y/n) [n]:"
	read -n 1 _answer
	echo
	if [[ $_answer != 'y' ]]; then
		return 1
	fi
	return 0
}

##
## Shows a multiple choice menu, with prompt specified in $1. Choices are passed as $2, and split using IFS.
## $3 is the name of the variable to store the choice name into.
##
function show_menu()
{
    (( $# == 3 )) || { echo_error "Expecting 3 parameters: PROMPT CHOICES VAR_NAME"; return 1; }

    _items=( $2 )
    _resultvar=$3

    if (( ${#_items[@]} == 1 )); then
        _result="${_items[0]}"
        eval $_resultvar="$_result"
        return
    fi

    echo_bold "$1:"
    local _count=0
    local _x
    local _choice
    local _result
    for _x in $2; do
        (( _count++ ))
        echo "$_count. $_x"
    done

    _valid_choice=0

    while (( $_valid_choice != 1 )); do
        read -p "Enter your choice: "   _choice
        _valid_choice=1
        (( _choice > 0 && _choice <= ${#_items[@]} )) 2>/dev/null || _valid_choice=0

        if (( _valid_choice == 0 )); then
            echo_error "Invalid choice: $_choice"
            continue
        fi

        (( _choice = _choice - 1 ))

    done

    _result="${_items[$_choice]}"
    eval $_resultvar="$_result"
}

##
## Exits script with error message specified in $1.
##
function exit_with_error()
{
    echo_error "$1"
    exit 1
}

##
## Verifies if the input path is an existing directoy. Exits with an error if the directory does not exist.
##
function verify_directory_exists()
{
    local _this_path="$1"
    [[ -d "$_this_path" ]] || exit_with_error "Directory does not exist: $_this_path"
    echo "Directory verified: $_this_path"
    return 0
}

##
## Verifies if the input file exists. Exits with an error if the file does not exist.
##
function verify_file_exists()
{
    local _this_path="$1"
    [[ -f "$_this_path" ]] || exit_with_error "File does not exist: $_this_path"
    echo "File verified: $_this_path"
    return 0
}

##
## Verifies if the input JSON file is formatted properly. Exits with an error if the file is not valid.
##
function verify_json_file()
{
    local _json_file_path="$1"
    ( cat "$_json_file_path" | python -m simplejson.tool >/dev/null )
    [[ $? -eq 0 ]] || exit_with_error "JSON file $_json_file_path could not be validated."
}


#==============================================================================
# Parsing functions
#==============================================================================

##
## Parses JSON key from file.
## - When 2 arguments provided:
##     parse key $2 from file $1
## - When 3 arguments provided:
##     parse key $2.$3 from file $1
##
function parse_json_key()
{
    if (( $# == 2 )); then
	    cat "$1" | python -c 'import sys, json; print json.load(sys.stdin)[sys.argv[1]]' $2
	elif (( $# == 3 )); then
	    cat "$1" | python -c 'import sys, json; print json.load(sys.stdin)[sys.argv[1]][sys.argv[2]]' $2 $3
	fi

}

##
## Parse the first level of JSON into bash variables.
## $1 contains the JSON file name, $2 contains the variable name prefix.
## { "var1": "value1"} -> var1="value1" -> $var1 has the value of 'value1'
##
function load_json_variables()
{
    [[ -n "$1" ]] || { echo_error "JSON file name not specified."; return 1; }
    [[ -n "$2" ]] || { echo_error "Variable names prefix not specified."; return 1; }
    _var_name_prefix="$2"
    _json_file_name="$1"
    eval `cat "$_json_file_name" | python -c '
import json; import sys;
data = json.load(sys.stdin);
def dump_data(data):
    if type(data) is list:
        return "( %s )" % "\n".join([json.dumps(str(item)) for item in data])
    else:
        return json.dumps(data)
entries = ["%s%s=%s" % (sys.argv[1], key, dump_data(data[key])) for key in data]; print "\n".join(entries)
' $_var_name_prefix`
}

##
## Parses the regex out of STDIN using Perl RE.
## $1 contains the expression, while $2 contains the printout:
##  - $0 -- entire matched string
##  - $n -- matching group
##
function regex_match()
{
    _pattern="$1"
    _printout="${2/\$0/\$&}"
    perl -ne 'if (m/'"$_pattern"'/) { print "'"$_printout"'\n"; }'
}


#==============================================================================
# Other convenience functions
#==============================================================================

##
## Joins all of the passsed arguments with the first character of
## string passed in $1.
##
function str_join()
{
    local IFS="$1"
    shift
    echo "$*"
}

##
## Generates a random 8-character string (hexadecimal characters only).
##
function generate_short_id()
{
	local _short_id=$( dd if=/dev/urandom count=128 bs=1 2>/dev/null | shasum )
	_short_id=${_short_id:0:8}
	echo $_short_id
}

##
## Generates a random 32-character string (hexadecimal characters only).
##
function generate_id()
{
	local _id=$( dd if=/dev/urandom count=128 bs=1 2>/dev/null | shasum )
	_id=${_id:0:32}
	echo $_id
}

##
## Retrieves the checked out git branch name in the current working directory.
##
function git_branch_name()
{
    git branch | grep "*" | sed "s/* //"
}

##
## Ensures that the path $1 is on branch $2. Exits the program if branches don't match.
##
function require_git_branch()
{
    local _validation_path="$1"
    local _required_git_branch="$2"

    git_branch=$( cd "$_validation_path" && git_branch_name )

    if [[ "$_required_git_branch" != "$git_branch" ]]; then
        exit_with_error "Incorrect git branch: $git_branch (expected $_required_git_branch)"
    fi

}

##
## Compare two input versions
## (Original from: http://stackoverflow.com/questions/4023830/bash-how-compare-two-strings-in-version-format)
##
## Return codes:
##   0: $1 = $2
##   1: $1 > $2
##   2: $1 < $2
##
## Examples:
##   1 = 1
##   2.1 < 2.2
##   3.0.4.10 > 3.0.4.2
##   4.08 < 4.08.01
##   3.2.1.9.8144 > 3.2
##   3.2 < 3.2.1.9.8144
##   1.2 < 2.1
##   2.1 > 1.2
##   5.6.7 = 5.6.7
##   1.01.1 = 1.1.1
##   1.1.1 = 1.01.1'
##   1 = 1.0
##   1.0 = 1
##   1.0.2.0 = 1.0.2
##   1..0 = 1.0
##   1.0 = 1..0
##
function compare_versions()
{
    if [[ $1 == $2 ]]
    then
        return 0
    fi
    local IFS=.
    local i ver1=($1) ver2=($2)
    # fill empty fields in ver1 with zeros
    for ((i=${#ver1[@]}; i<${#ver2[@]}; i++))
    do
        ver1[i]=0
    done
    for ((i=0; i<${#ver1[@]}; i++))
    do
        if [[ -z ${ver2[i]} ]]
        then
            # fill empty fields in ver2 with zeros
            ver2[i]=0
        fi
        if ((10#${ver1[i]} > 10#${ver2[i]}))
        then
            return 1
        fi
        if ((10#${ver1[i]} < 10#${ver2[i]}))
        then
            return 2
        fi
    done
    return 0
}

#==============================================================================
# Path functions
#==============================================================================

##
## Resolves an absolute path from a relative path. Follows symlinks.
## Original source: https://github.com/keen99/shell-functions/blob/master/resolve_path/resolve_path.inc
##
function resolve_path() {
	local _owd="$PWD"
	local _opath="$1"
	local _npath=""
	local _obase=$(basename "$_opath")
	local _odir=$(dirname "$_opath")
	if [[ -L "$_opath" ]]
	then
	# it's a link.
	# file or directory, we want to cd into it's dir
		cd $_odir
	# then extract where the link points.
		_npath=$(readlink "$_obase")
		# have to -L BEFORE we -f, because -f includes -L :(
		if [[ -L $_npath ]]
		 then
		#the link points to another symlink, so go follow that.
			resolve_path "$_npath"
			#and finish out early, we're done.
			return $?
			#done
		elif [[ -f $_npath ]]
		# the link points to a file.
		 then
			# get the dir for the new file
			_nbase=$(basename $_npath)
		 	_npath=$(dirname $_npath)
		 	cd "$_npath"
		 	_ndir=$(pwd -P)
		 	_retval=0
		 	#done
		elif [[ -d $_npath ]]
		 then
		# the link points to a directory.
			cd "$_npath"
			_ndir=$(pwd -P)
			_retval=0
			# done
		else
			echo "$FUNCNAME: ERROR: unknown condition inside link!!" >&2
			echo "opath [[ $_opath ]]" >&2
			echo "npath [[ $_npath ]]" >&2
			return 1
		fi
	else
		if ! [[ -e "$_opath" ]]
		 then
			echo "$FUNCNAME: $_opath: No such file or directory" >&2
			return 1
			#and break early
		elif [[ -d "$_opath" ]]
		 then
			cd "$_opath"
			_ndir=$(pwd -P)
			_retval=0
			#done
		elif [[ -f "$_opath" ]]
		 then
		 	cd $_odir
		 	_ndir=$(pwd -P)
		 	_nbase=$(basename "$_opath")
		 	_retval=0
		 	#done
		else
			echo "$FUNCNAME: ERROR: unknown condition outside link!!" >&2
			echo "opath [[ $_opath ]]" >&2
			return 1
		fi
	fi
	# now assemble our output
	echo -n "$_ndir"
	if [[ "x${_nbase:=}" != "x" ]]
	 then
	 	echo "/$_nbase"
	else
		echo
	fi
	# now return to where we were
	cd "$_owd"
	return $_retval
}

#==============================================================================
# URL functions
#==============================================================================

function raw_url_encode() {
  local string="${1}"
  local strlen=${#string}
  local encoded=""
  local pos c o

  for (( pos=0 ; pos<strlen ; pos++ )); do
     c=${string:$pos:1}
     case "$c" in
        [-_.~a-zA-Z0-9] ) o="${c}" ;;
        * )               printf -v o '%%%02x' "'$c"
     esac
     encoded+="${o}"
  done
  echo "${encoded}"
}


#==============================================================================
# Remote services functions
#==============================================================================

##
## Sends a notification via Pushover notification service.
## $1: message contents
## $2 (optional): message title.
##
function pushover()
{

    [[ -n "$PUSHOVER_USER_KEY" ]] || { echo_error "PUSHOVER_USER_KEY required!"; return 1; }
    [[ -n "$PUSHOVER_TOKEN" ]] || { echo_error "PUSHOVER_TOKEN required!"; return 1; }

    local _title=${2:-`hostname`}
    local _message="$1"

    curl -s -F "token=$PUSHOVER_TOKEN" \
    -F "user=$PUSHOVER_USER_KEY" \
    -F "title=$_title" \
    -F "message=$_message" https://api.pushover.net/1/messages.json >/dev/null &

    # TODO: Add erroneous exit code if push fails.

    return 0
}


#==============================================================================
# Dependencies check run
#==============================================================================

function bash_helpers_check_dependencies()
{
    local _utility_dependency _python_module_dependency
    for _utility_dependency in "${PXBH_UTILITIES_DEPENDENCIES[@]}"
    do
        which $_utility_dependency >/dev/null || exit_with_error "Utility not found in the search path: $_utility_dependency"
    done

    installed_python_modules=$( pip freeze )

    for _python_module_dependency in "${PXBH_PYTHON_MODULES_DEPENDENCIES[@]}"
    do
        (
            echo $installed_python_modules | grep "$_python_module_dependency=="
        ) >/dev/null || exit_with_error "Python module not installed: $_python_module_dependency"
    done
}

bash_helpers_check_dependencies
