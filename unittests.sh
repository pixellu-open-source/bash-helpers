#!/usr/bin/env bash

. ./px-bash-helpers.sh

which shunit2 >/dev/null || exit_with_error "shunit2 required for running unit tests. See https://code.google.com/p/shunit2/"

function testResolvePath()
{
    result=$( resolve_path unittests.sh )
    assertEquals "${PWD}/unittests.sh" "$result"
}

function testStrings()
{

    local result=`str_join ";" "string1" "string2" "string3"`
    assertEquals "string1;string2;string3" $result

}

function testMenu()
{

    local choices="choice1"

    show_menu "Make a choice, please" "$choices" user_choice
    assertEquals "choice1" "$user_choice"


}
function testJSON()
{

    json_contents='
        {
          "var1": "value \"1\"",
          "var2": 2,
          "var3": 3.5,
          "var4": true,
          "var5": false,
          "var6": [5, 6, "test 7"]
        }'

    echo "$json_contents" >.unittests-testJSON.json
    load_json_variables .unittests-testJSON.json my_vars_
    rm .unittests-testJSON.json

    assertEquals "$my_vars_var1" "value \"1\""
    assertEquals "$my_vars_var2" "2"
    assertEquals "$my_vars_var3" "3.5"
    assertEquals "$my_vars_var4" "true"
    assertEquals "$my_vars_var5" "false"
    assertEquals "${my_vars_var6[0]}" "5"
    assertEquals "${my_vars_var6[1]}" "6"
    assertEquals "${my_vars_var6[2]}" "test 7"

}

function testRegex()
{

    local regex_contents='
test12345
 test12345
test
12345test
test2
test34
'
    local old_ifs="$IFS"
    local IFS=$'\n'
    local matches=( `echo "$regex_contents" | regex_match '^te(st\d{2}).*' '$0 $1'` )
    IFS="$old_ifs"

    assertEquals "test12345 st12" "${matches[0]}"
    assertEquals "test34 st34" "${matches[1]}"

}

. "shunit2"

