PREFIX=/usr/local

test:
	./unittests.sh

install:
	cp -f px-bash-helpers.sh "$(PREFIX)/bin/px-bash-helpers.sh"

uninstall:
	rm -f "$(PREFIX)/bin/px-bash-helpers.sh"
